import { useState, useEffect } from 'react';
import List from './List';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

interface ITodo {
  id: string;
  title: string;
}

const getLocalStorage = (): ITodo[] => {
  const list = localStorage.getItem("list");
  if (list) {
    return JSON.parse(list);
  } else {
    return [];
  }
}

function App() {
  const [name, setName] = useState<string>("");
  const [list, setList] = useState<ITodo[]>(getLocalStorage());
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [editID, setEditID] = useState<string | undefined>();

  useEffect(() => {
    localStorage.setItem("list", JSON.stringify(list));
  }, [list]);

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!name) {//checks if title field is not empty
      alert("Title Field Cannot Be Empty!!")
    } else if (name && isEditing) {//for update purpose
      const updatedList = list.map((item) =>
        item.id === editID ? { ...item, title: name } : item
      );
      setList(updatedList);
      setEditID(undefined);
      setIsEditing(false);

    } else {//this will add new notes
      const newItem: ITodo = { id: Math.random().toString().substring(15), title: name };
      setList([...list, newItem]);
    }
    setName("");
  };

  const removeItem = (id: string): void => {
    setList(list.filter((item) => item.id !== id))
  }
  const editItem = (id: string): void => {
    const editItem = list.find((item) => item.id === id);
    setIsEditing(true);
    setEditID(id);
    setName(editItem?.title || "");
  }
  const clearList = (): void => {
    setList([]);
  }

  return (
    <section className='section-center'>
      <form onSubmit={handleSubmit}>

        <h3 className='main-head'>Todo-List App</h3>
        <div className='mb-3 form'>
          <input type='text' className='inp-control' placeholder='e.g Homework' onChange={(e) => setName(e.target.value)} value={name} autoFocus />{" "}
          <button type='submit' className='smbt-btn'>
            {isEditing ? "Edit" : "Submit"}
          </button>
        </div>
      </form>
      {list.length > 0 && (
        <div>
          <List items={list} removeItem={removeItem} editItem={editItem} />
          <div className='text-center'>
            <button className='delete-btn' onClick={clearList}>Clear All</button>
          </div>
        </div>
      )}
    </section>
  );
}

export default App;
